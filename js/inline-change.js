(function ($, Drupal) {
  Drupal.behaviors.inline_change = {
    attach: function (context, settings) {
      once('trinion-inline-change', '.t-changed-table', context).forEach(function (element) {
        $(".t-changed-table .inline-change").dblclick(function(){
          var id = $(this).closest('tr').data('id');
          var type = $(this).closest('tr').data('type');
          var field = $(this).data('field');
          Drupal.ajax({
            url: `/inline-show-field/${type}/${id}/${field}`,
          }).execute().done(function () {
            setTimeout(function(){
              $(".t-changed-table input[type='text']").each(function(){
                if ($(this).data('autocomplete-path')) {
                  $(this).autocomplete({
                    source: $(this).data('autocomplete-path'),
                    select: function(event, ui) {
                      var el = $(this);
                      setTimeout(function(){
                        change_handler(el);
                      }, 100);
                    }
                  });
                }
              });
              setTimeout(function() {
                var select = $('.t-changed-table select[multiple]');
                if (select.length) {
                  var tomselect = select.eq(0)[0].tomselect;
                  tomselect.on('blur', function () {
                    change_handler(select);
                    get_handler(select);
                  });
                }
              }, 100);
              $(".tooltip").remove();
            }, 100);
          });
        });

      });
      function change_handler(el) {
        el.attr('disabled', 'disabled');
        var id = el.closest('tr').data('id');
        var type = el.closest('tr').data('type');
        var field = el.closest('td').data('field');
        if (el.attr('type') == 'checkbox')
          var data = el.prop('checked') ? 1 : 0;
        else
          var data = el.val();

        Drupal.ajax({
          url: `/inline-change-field/${type}/${id}/${field}`,
          submit: {
            data: data
          }
        }).execute();
      }

      function get_handler(el) {
        var id = el.closest('tr').data('id');
        var type = el.closest('tr').data('type');
        var field = el.closest('td').data('field');
        if (el.attr('type') == 'checkbox')
          var data = el.prop('checked') ? 1 : 0;
        else
          var data = el.val();

        Drupal.ajax({
          url: `/inline-get-value/${type}/${id}/${field}`,
          submit: {
            data: data
          }
        }).execute();
      }
      $(".views-form > form").keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });

      $(".t-changed-table select:not([multiple])").change(function(){
        change_handler($(this));
      }).blur(function(){
        get_handler($(this));
      }).keyup(function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
          change_handler($(this));
        }
      });
      $(".t-changed-table input[type='text']").blur(function(){
        change_handler($(this));
      }).keyup(function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
          change_handler($(this));
        }
      });
      $(".t-changed-table .inline-change input[type='checkbox']").change(function(){
        change_handler($(this));
      });
    }
  };
})(jQuery, Drupal);
