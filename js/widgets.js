(function ($, Drupal) {
  Drupal.behaviors.widgets = {
    attach: function (context, settings) {
      // once('body', '.t-changed-table', context).forEach(function (element) {
        $(".my-all-toggler").change(function(){
          var widget_id = $(this).closest(".widget-wrapper").data("widget-id");
          var val = $(this).prop("checked");
          Drupal.ajax({
            url: `/toggle-widget/${widget_id}/${val}`
          }).execute();
        });
      // });
    }
  };
})(jQuery, Drupal);
