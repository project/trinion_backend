(function ($, Drupal) {
  Drupal.behaviors.tom_select_init = {
    attach: function (context, settings) {
      var el;
      $("select[multiple]").each(function(){
        window.TomSelect && !$(this).hasClass('tomselected') && (new TomSelect(el = $(this), {
          copyClassesToDropdown: false,
          dropdownParent: 'body',
          controlInput: '<input>',
          render:{
            item: function(data,escape) {
              if( data.customProperties ){
                return '<div><span class="dropdown-item-indicator">' + data.customProperties + '</span>' + escape(data.text) + '</div>';
              }
              return '<div>' + escape(data.text) + '</div>';
            },
            option: function(data,escape){
              if( data.customProperties ){
                return '<div><span class="dropdown-item-indicator">' + data.customProperties + '</span>' + escape(data.text) + '</div>';
              }
              return '<div>' + escape(data.text) + '</div>';
            }
          }
        }));
      });
      $("select").each(function(){
        if ($(this).attr('multiple') == undefined) {
          !$(this).hasClass('tomselected') && (new TomSelect($(this),{
              create: true,
              sortField: {
                field: "text",
                direction: "asc"
              }
            })
          );
        }
      });
    }
  };
})(jQuery, Drupal);
