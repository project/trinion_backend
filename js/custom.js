(function ($, Drupal) {
  Drupal.behaviors.custom = {
    attach: function (context, settings) {
      $(".user-login-form .link-secondary").click(function(e){
        e.preventDefault();
        var input = $(this).closest(".input-group").find("input");
        var type = input.attr("type");
        if (type == "text")
          input.attr("type", 'password');
        else
          input.attr("type", 'text');
      });

      var fv_list = '';
      var fv = $("#filter-values .card-body .msgs");
      if (fv.length) {
        $(".views-exposed-form .form-item").each(function () {
          var label = $(this).find('label').html();
          if (label) {
            var select = $(this).find('select');
            if (select.length) {
              var select_name = select.attr("name");
              if (select_name != "items_per_page") {
                var val = select.val();
                if (val != 'All') {
                  val = select.find(`option[value='${val}']`).html();
                  if (fv_list)
                    fv_list = fv_list + ', ';
                  fv_list = fv_list + `<b>${label}</b>: ${val}`;
                }
              }
            } else {
              var input = $(this).find('input');
              var val = input.val();
              if (val) {
                if (fv_list)
                  fv_list = fv_list + ', ';
                fv_list = fv_list + `<b>${label}</b>: ${val}`;
              }
            }
          }
        });
        if (fv_list) {
          fv.html(fv_list);
          $("#filter-values").removeClass("d-none");
        }
        else
          $("#filter-values").addClass("d-none");
      }


      $("#edit-field-tz-proekt").mousedown(function(){
        $("#edit-field-tz-kategoriya-zadachi").val("_none");
      });
      if ($("#fast-switch-project").length > 0) {
        $("#fast-switch-project").val($("select[data-drupal-selector='edit-field-tz-proekt-target-id']").val());
        $("#fast-switch-project").change(function () {
          var proekt = $(this).val();
          Cookies.set("proekt", proekt);
          $("select[data-drupal-selector='edit-field-tz-proekt-target-id']").val(proekt);
          $(".views-exposed-form input[type='submit']").click();
        });
      }
      $("#edit-field-tp-schet-dlya-0-target-id").on("autocompleteopen", function(){
        $("[data-drupal-selector='edit-field-tl-contact']").val("_none");
      });
      if ($('#show-filter').length) {
        if ($($('#show-filter').data('bs-target')).length == 0)
          $('#show-filter').hide();
      }
      $(document).keydown(function(e) {
        var keyCode = e.keyCode;
        if (keyCode == 115 && $("#modal-fast-task").length > 0) {
          $("#modal-fast-task").modal("show");
        }
      });

      $(".node-attache-file").click(function(e){
        e.preventDefault();
        if ($("#trinion-crm-file-upload #edit-file").length)
          $("#trinion-crm-file-upload #edit-file").click();
        if ($("#trinion-zadachnik-file-upload #edit-file").length)
          $("#trinion-zadachnik-file-upload #edit-file").click();
      });
      $(".replay-comment").click(function(){
        var textarea = $("textarea[data-drupal-selector='edit-field-tz-kommentariy-0-value']");
        textarea.val(textarea.val() + '#' + $(this).data('cid') + "\n");
        document.getElementById('edit-field-tz-kommentariy-0-value').scrollIntoView();
        textarea.focus();
      });
      $(".task-comment-attach").click(function(){
        $("#comment-form .form-file").click();
      });
      once('trinion-custom', 'body', context).forEach(function (element) {
        var menu_state = localStorage.getItem('left_menu_hide');
        if (menu_state == 'true') {
          $("aside.navbar").addClass("minified-menu");
          $("body").addClass("menu-collapsed");
        }
        $(".navbar .navbar-toggle").click(function (e) {
          e.preventDefault();
          $("aside.navbar").toggleClass("minified-menu");
          $("body").toggleClass("menu-collapsed");
          var menu_state = localStorage.getItem('left_menu_hide');
          localStorage.setItem('left_menu_hide', menu_state == 'false' ? 'true' : 'false' );
        });
        $(".document-list .views-row").click(function(){
          var id = $(this).data('nid');
          Drupal.ajax({url: `/view-document-callback/${id}`}).execute();
          history.pushState(null, '', `/node/${id}`);
          $(".document-list .views-row").removeClass('selected');
          $(this).addClass('selected');
        });
        $(".launch.icon.item").click(function(){
          $("#left-menu").toggle();
        });
        if ($('#lesson-player').length > 0)
          var player = new Plyr('#lesson-player');

        $(".new-notices-switcher").change(function(){
          var val = $(this).prop("checked");
          Drupal.ajax({url: `/new-notices-switcher/${val}`}).execute();
        });
      });
      $(".my-all-switcher").change(function(){
        var list = $(this).data('list');
        var val = $(this).prop("checked");
        Drupal.ajax({url: `/my-all-switcher/${list}/${val}`}).execute().done(function(){
          $(".views-exposed-form .form-submit").click();
        });
      });
    }
  };
})(jQuery, Drupal);
